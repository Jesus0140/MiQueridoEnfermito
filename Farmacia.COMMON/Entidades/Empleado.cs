﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidades
{
    public class Empleado:Base
    {
        
        public string NombreEmpleado { get; set; }
        public string Sexo { get; set; }
        public override string ToString()
        {
            return string.Format("{0} ({1}) ", NombreEmpleado, Sexo);
        }
    }
}
