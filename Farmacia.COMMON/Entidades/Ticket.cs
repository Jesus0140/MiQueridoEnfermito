﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidades
{
    public class Ticket:Base
    {
       
       public DateTime? FechaHoraVenta { get; set; }
        //material==producto
        public List<Producto> Poducto { get; set; }
        // solicitante== Vendedor
        public Empleado Vendedor { get; set; }
        //empleado==cliente
        // encargadoDeAlmacen == clienteCompra
        public Cliente ClienteCompra { get; set; }
        public List<Producto> ListaProductos { get; set; }
    }
}
