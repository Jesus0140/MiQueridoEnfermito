﻿using Farmacia.BIZ;
using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using Farmacia.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Farmacia.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        enum accion
        {
            Nuevo,
            Editar
        }

        IManejadorEmpleados manejadorEmpleados;
        IManejadorClientes manejadorClientes;
        IManejadorProductos manejadorProductos;

        accion accionEmpleados;
        accion accionClientes;
        accion accionProductos;

        public MainWindow()
        {
            InitializeComponent();
            manejadorEmpleados = new ManejadorEmpleados(new RepositorioEmpleados());
            manejadorProductos = new ManejadorProductos(new RepositorioProductos());
            manejadorClientes = new ManejadorClientes(new RepositorioClientes());

            PonerBotonesEmpleadosEnEdicion(false);
            LimpiarCamposDeEmpleados();
            ActualizarTablaEmpleados();

            PonerBotonesProductosEnEdicion(false);
            LimpiarCamposDeProductos();
            ActualizarTablaDeProductos();


            PonerBotonesClientesEnEdicion(false);
            LimpiarCamposDeClientes();
            ActualizarTablaDeClientes();

        }

        private void ActualizarTablaDeClientes()
        {
            dtgClientes.ItemsSource = null;
            dtgClientes.ItemsSource = manejadorClientes.Listar;
        }

        private void LimpiarCamposDeClientes()
        {

            txbClientesNombre.Clear();
            txbClientesApellidos.Clear();
            txbclientesDireccion.Clear();
            txbclientesRFC.Clear();
            txbclientesEmail.Clear();
            txbclientesTelefono.Clear();
            txbClientesId.Text = "";
        }

        private void PonerBotonesClientesEnEdicion(bool value)
        {
            btnClientesCancelar.IsEnabled = value;
            btnClientesEditar.IsEnabled = !value;
            btnClientesEliminar.IsEnabled = !value;
            btnClientesGuardar.IsEnabled = value;
            btnClientesNuevo.IsEnabled = !value;
        }

        private void ActualizarTablaDeProductos()
        {
            dtgProductos.ItemsSource = null;
            dtgProductos.ItemsSource = manejadorProductos.Listar;
        }

        private void LimpiarCamposDeProductos()
        {
            txbProductosNombre.Clear();
            txbProductosCategoria.Clear();
            txbProductosDescripcion.Clear();
            txbProductosPrecioV.Clear();
            txbProductosC.Clear();
            txbProductoE.Clear();
            txbProductosId.Text = "";

        }

        private void PonerBotonesProductosEnEdicion(bool value)
        {
            btnProductosCancelar.IsEnabled = value;
            btnProductosEditar.IsEnabled = !value;
            btnProductosEliminar.IsEnabled = !value;
            btnProductosGuardar.IsEnabled = value;
            btnProductosNuevo.IsEnabled = !value;
        }

        private void ActualizarTablaEmpleados()
        {
            dtgEmpleados.ItemsSource = null;
            dtgEmpleados.ItemsSource = manejadorEmpleados.Listar;
        }

        private void LimpiarCamposDeEmpleados()
        {
            txbEmpleadosId.Text = "";
            txbEmpleadosNombre.Clear();
            txbEmpleadosSexo.Clear();
        }

        private void PonerBotonesEmpleadosEnEdicion(bool value)
        {
            btnEmpleadosCancelar.IsEnabled = value;
            btnEmpleadosEditar.IsEnabled = !value;
            btnEmpleadosEliminar.IsEnabled = !value;
            btnEmpleadosGuardar.IsEnabled = value;
            btnEmpleadosNuevo.IsEnabled = !value;
        }

        private void btnEmpleadosNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadosEnEdicion(true);
            accionEmpleados = accion.Nuevo;

        }

        private void btnEmpleadosEditar_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleados.SelectedItem as Empleado;
            if (emp != null)
            {
                txbEmpleadosId.Text = emp.Id;
                txbEmpleadosSexo.Text = emp.Sexo;
               
                
                accionEmpleados = accion.Editar;
                PonerBotonesEmpleadosEnEdicion(true);
            }

        }

        private void btnEmpleadosGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionEmpleados == accion.Nuevo)
            {
                Empleado emp = new Empleado()
                {
                    NombreEmpleado = txbEmpleadosNombre.Text,
                  
                   Sexo = txbEmpleadosSexo.Text
                };
                if (manejadorEmpleados.Agregar(emp))
                {
                    MessageBox.Show("Empleado agregado correctamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEmpleados();
                    ActualizarTablaEmpleados();
                    PonerBotonesEmpleadosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Empleado no se pudo agregar", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Empleado emp = dtgEmpleados.SelectedItem as Empleado;
                emp.Sexo = txbEmpleadosSexo.Text;
                
                emp.NombreEmpleado = txbEmpleadosNombre.Text;
                if (manejadorEmpleados.Modificar(emp))
                {
                    MessageBox.Show("Empleado modificado correctamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEmpleados();
                    ActualizarTablaEmpleados();
                    PonerBotonesEmpleadosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Empleado no se pudo actualizar", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }


        }

        private void btnEmpleadosCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadosEnEdicion(false);

        }

        private void btnEmpleadosEliminar_Click(object sender, RoutedEventArgs e)
        {

            Empleado emp = dtgEmpleados.SelectedItem as Empleado;
            if (emp != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este empleado?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorEmpleados.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Empleado eliminado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaEmpleados();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el empleado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }


        }









        private void btnProductosNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeProductos();
            accionProductos = accion.Nuevo;
            PonerBotonesProductosEnEdicion(true);

        }

        private void btnProductosEditar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeProductos();
            accionProductos = accion.Editar;
            PonerBotonesProductosEnEdicion(true);
            Producto m = dtgProductos.SelectedItem as Producto;
            if (m != null)
            {
                txbProductosNombre.Text = m.Mercancia;
                txbProductosCategoria.Text = m.Categoria;
                txbProductosId.Text = m.Id;
                txbProductosDescripcion.Text = m.Descripcion;
                txbProductosPrecioV.Text = m.PrecioV;
                txbProductosC.Text = m.PrecioC;
                txbProductoE.Text = m.ProductoE;
            }

        }

        private void btnProductosGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionProductos == accion.Nuevo)
            {
                Producto m = new Producto()
                {
                    Mercancia = txbProductosNombre.Text,
                    Categoria = txbProductosCategoria.Text,
                    Descripcion = txbProductosDescripcion.Text,
                    PrecioV = txbProductosPrecioV.Text,
                    ProductoE= txbProductoE.Text,
                    PrecioC= txbProductosC.Text
                };
                if (manejadorProductos.Agregar(m))
                {
                    MessageBox.Show("Producto correctamente agregado", "Administrador", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeProductos();
                    ActualizarTablaDeProductos();
                    PonerBotonesProductosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("Algo salio mal", "Administrador", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Producto m = dtgProductos.SelectedItem as Producto;

                m.Mercancia = txbProductosNombre.Text;
                m.Categoria = txbProductosCategoria.Text;
                m.Descripcion = txbProductosDescripcion.Text;
                m.PrecioV = txbProductosPrecioV.Text;
                m.ProductoE = txbProductoE.Text;
                m.PrecioC = txbProductosC.Text;

                if (manejadorProductos.Modificar(m))
                {
                    MessageBox.Show("Producto correctamente modificado", "Administrador", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeProductos();
                    ActualizarTablaDeProductos();
                    PonerBotonesProductosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("Algo salio mal", "Administrador", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }

        private void btnProductosCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeProductos();
            PonerBotonesProductosEnEdicion(false);

        }

        private void btnProductosEliminar_Click(object sender, RoutedEventArgs e)
        {
            Producto m = dtgProductos.SelectedItem as Producto;
            if (m != null)
            {
                if (MessageBox.Show("¿Realmente deseas eliminar este Producto?", "Productos", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorProductos.Eliminar(m.Id))
                    {
                        MessageBox.Show("Producto Eliminado correctamente", "Productos", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaDeProductos();
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Producto", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }

        }





        private void btnClientesNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeClientes();
            PonerBotonesClientesEnEdicion(true);
            accionClientes = accion.Nuevo;

        }

        private void btnClientesEditar_Click(object sender, RoutedEventArgs e)
        {
            Cliente empi = dtgClientes.SelectedItem as Cliente;
            if (empi != null)
            {
                txbClientesId.Text = empi.Id;
                txbClientesNombre.Text = empi.Nombre;
                txbClientesApellidos.Text = empi.Apellidos;
                txbclientesDireccion.Text = empi.Direccion;
                txbclientesRFC.Text = empi.RFC;
                txbclientesTelefono.Text = empi.Telefono;
                txbclientesEmail.Text = empi.Email;


               accionClientes = accion.Editar;
                PonerBotonesClientesEnEdicion(true);
            }

        }

        private void btnClientesGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionClientes == accion.Nuevo)
            {
                Cliente empi = new Cliente()
                {
                    Nombre = txbClientesNombre.Text,
                    Apellidos = txbClientesApellidos.Text,
                    Direccion=txbclientesDireccion.Text,
                    RFC=txbclientesRFC.Text,
                    Telefono=txbclientesTelefono.Text,
                    Email = txbclientesEmail.Text
                };
                if (manejadorClientes.Agregar(empi))
                {
                    MessageBox.Show("Cliente agregado correctamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeClientes();
                    ActualizarTablaDeClientes();
                    PonerBotonesClientesEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Cliente no se pudo agregar", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Cliente empi = dtgClientes.SelectedItem as Cliente;

             
                empi.Nombre = txbClientesNombre.Text;
                empi.Apellidos = txbClientesApellidos.Text;
                empi.Direccion = txbclientesDireccion.Text;
                empi.RFC = txbclientesRFC.Text;
                empi.Telefono = txbclientesTelefono.Text;
                empi.Email = txbclientesEmail.Text;
               

                if (manejadorClientes.Modificar(empi))
                {
                    MessageBox.Show("Cliente modificado correctamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeClientes();
                    ActualizarTablaDeClientes();
                    PonerBotonesClientesEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Cliente no se pudo actualizar", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }



        }

        private void btnClientesCancelar_Click(object sender, RoutedEventArgs e)
        {

            LimpiarCamposDeClientes();
            PonerBotonesClientesEnEdicion(false);
        }

        private void btnClientesEliminar_Click(object sender, RoutedEventArgs e)
        {

            Cliente empi = dtgClientes.SelectedItem as Cliente;
            if (empi != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este Cliente?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorClientes.Eliminar(empi.Id))
                    {
                        MessageBox.Show("Cliente eliminado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaDeClientes();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Cliente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }

        }
    }
}
