﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Farmacia.GUI.Inicio
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void btnventa_Click(object sender, RoutedEventArgs e)
        {

          Farmacia.GUI.Venta.MainWindow NuevaVentana = new Farmacia.GUI.Venta.MainWindow();
         NuevaVentana.Show();

        }

        private void btnProductos_Click(object sender, RoutedEventArgs e)
        {
            Farmacia.GUI.Administrador.MainWindow NuevaVentana = new Farmacia.GUI.Administrador.MainWindow();
            NuevaVentana.Show();


        }
    }
}
