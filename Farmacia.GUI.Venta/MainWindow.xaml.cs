﻿using Farmacia.BIZ;
using Farmacia.COMMON.Entidades;
using Farmacia.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Farmacia.GUI.Venta
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }

        accion accionVenta;
    


        ManejadorEmpleados manejadorDeEmpleados;
        ManejadorTickets manejadorDeTickets;
        ManejadorProductos manejadorProductos;
        ManejadorClientes manejadorClientes;
       
        public MainWindow()
        {
            InitializeComponent();
            manejadorDeEmpleados = new ManejadorEmpleados(new RepositorioEmpleados());
            manejadorDeTickets = new ManejadorTickets(new RepositorioTickets());
            manejadorProductos = new ManejadorProductos(new RepositorioProductos());
            manejadorClientes = new ManejadorClientes(new RepositorioClientes());
            ActualizarTablaVenta();
            BloquearBotones();

        }

        private void BloquearBotones()
        {
            btnAgregar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            btnComprar.IsEnabled = false;
            btnNuevo.IsEnabled = true;
          
            cmbEmpleado.IsEnabled = false;
            cmbNombreCliente.IsEnabled = false;
            cmbProducto.IsEnabled = false;
        }

        private void ActualizarTablaVenta()
        {
            dtgVenta.ItemsSource = null;
            dtgVenta.ItemsSource = manejadorDeTickets.Listar;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {

            cajas();
            accionVenta = accion.Nuevo;

            btnAgregar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            btnComprar.IsEnabled = true;
            btnNuevo.IsEnabled = true;
         
            cmbEmpleado.IsEnabled = true;
            cmbNombreCliente.IsEnabled = true;
            cmbProducto.IsEnabled = true;

        }

        private void cajas()
        {
            cmbProducto.ItemsSource = null;
            cmbProducto.ItemsSource = manejadorProductos.Listar;

            cmbNombreCliente.ItemsSource = null;
            cmbNombreCliente.ItemsSource = manejadorClientes.Listar;

            cmbEmpleado.ItemsSource = null;
            cmbEmpleado.ItemsSource = manejadorDeEmpleados.Listar;
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {

            MessageBox.Show("El ticket ha sido generado", "venta", MessageBoxButton.OK, MessageBoxImage.Information);

        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {

            LimpiarCampos();




        }

        private void LimpiarCampos()
        {
            cmbProducto.ItemsSource = null;
            cmbNombreCliente.ItemsSource = null;
            cmbEmpleado.ItemsSource = null;
            txbCantidad.Text = null;
        }




       


        private void btnComprar_Click(object sender, RoutedEventArgs e)
        {

            if (accionVenta == accion.Nuevo)
            {
                Ticket emp = new Ticket()
                {
                    ClienteCompra = cmbNombreCliente.ItemsSource as Cliente,
                    Vendedor = cmbEmpleado.ItemsSource as Empleado,
                  


                   
                };
                if (manejadorDeTickets.Agregar(emp))
                {
                    MessageBox.Show("La compra  fue exitosa", "venta", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCampos();
                    ActualizarTablaVenta();
                    BloquearBotones();
                }
               
            }
          




        }

        private void PonerBotonesEmpleadosEnEdicion(bool value)
        {
            btnCancelar.IsEnabled = value;
            btnAgregar.IsEnabled = !value;
          
            btnComprar.IsEnabled = value;
            btnNuevo.IsEnabled = !value;
        }
    }
    }

